from distutils.core import setup

__author__ = 'taylor'

setup(
    name='autopg',
    version='0.0.0.2',
    decription='Wrapper for psycopg2 that make operations simple and thread/process safe',
    author='Taylor Lanclos',
    packages=[
        'autopg'
    ]
)