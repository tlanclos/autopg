autopg module
=============

.. automodule:: autopg
    :members:
    :undoc-members:
    :show-inheritance:

=============

An example use of this module is::

    from autopg import autopg
    db = autopg(
        dbname='dbname', 
        user='user', 
        password='password'
    )
    with db as cursor:
        _params = {
            'value': 'myvalue',
            'tag': 'mytag'
        }
        cursor.execute('UPDATE schema.table SET label = %(value)s WHERE tag = %(tag)s', _params)

        _resultset = cursor.execute('SELECT * FROM schema.table WHERE label = %s', ('my_label',))

        for result in _resultset:
            print(result)

Note that the above example will only protect the connection among threads with the current configuration, by simply
specifying a different `lock_factory`, one can make this class safe among processes::

    from autopg import autopg
    from namedlock import namedlock
    db = autopg(
        dbname='dbname', 
        user='user', 
        password='password', 
        lock_factory=namedlock, 
        lock_kwargs={'filename': 'dbprotector.lock'}
    )
    ...

To demonstrate an example of a full python module that uses this code, one can create multiple processes that use different
connections, but the same lock object. This is extremely useful in modules that spawn multiple processes, but would require
protection on the connection. In the following code, you will see that while `f1` executes its selects almost immediately,
`f2` and `f3` will go hand-in-hand waiting for each other to finish before executing their respective transaction.
You may also note that the transaction in `f1` is not committed at any point. You may also notice that the with statement
in `f2` has "with db.safe as cursor" while the one in `f3` has "with db as cursor". These mean exactly the same thing.::

    from autopg import autopg
    from autopg import namedlock
    from multiprocessing import Process

    def f1():
        db = autopg(dbname='dbname', user='user', password='password', lock_factory=namedlock, lock_args=('mylock',))
        for i in range(3):
            with db.unsafe as cursor:
                print('unsafe cursor!')
                for r in cursor.execute('select * from schema.table'):
                    print(r)
            print('exited unsafe cursor!')

    def f2():
        db = autopg(dbname='dbname', user='user', password='password', lock_factory=namedlock, lock_args=('mylock',))
        for i in range(3):
            with db.safe as cursor:
                print('started safe transaction f2')
                for r in cursor.execute('select * from schema.table where id=20'):
                    print(r)
                print('ending safe transaction f2')

    def f3():
        db = autopg(dbname='dbname', user='user', password='password', lock_factory=namedlock, lock_args=('mylock',))
        for i in range(3):
            with db as cursor:
                print('started safe transaction f3')
                cursor.execute('update schema.table set tag=%s where id=20', ('mytag',))
                print('ending safe transaction f3')

    p1 = Process(target=f1)
    p2 = Process(target=f2)
    p3 = Process(target=f3)

    p2.start()
    p3.start()
    p1.start()
