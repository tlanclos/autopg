# autopg
---
## Contributers:

- Taylor Lanclos

## Background

> I work with psycopg2 quite a bit because I find it unnecessary to use an ORM when writing small scripts that do specific tasks.
> After writing a couple of these scripts, I found myself frustrated with how non-elegant psycopg2's framework was. I know the
> reason for this is because psycopg2 is a wrapper around the C library `libpq.so` and it can be difficult to write a very generic
> pythonic system off of a C library-which I also have a bit of experience with. With this, I had the idea of writing a library to wrap
> the functionality of psycopg2 in a more elegant and easy to use framework.

## Synopsis

> This library is essentially a wrapper for psycopg2 that make operations simple and thread/process safe. Anytime information needs to be
> retrieved or update, the entire connection is locked. This means that no other thread can execute a query on that connection.
> Yes, this may make operations slow if you are constantly updating and selecting stuff from the database you are connected to,
> but if that is the case, you may want to write procedures for your database that explicitly lock rows that are being updated/selected.
> For my purposes, I consider any access to the database a critical section of your application. Since I consider this critical,
> if there are any unhandled exceptions within the execution of a transaction, I automatically rollback any changes that were made.
> For example:
``` Python
db = autopg(dbname='dbname', user='user', password='password')
with db as cursor:
    cursor.execute('UPDATE schema.table SET value = 30')
    cursor.execute('SELECT * FROM schema.table')
```
> If the previous code threw an exception while attempting to update or select from `schema.table`, a rollback occurs. Doing this protects
> the database from a badly written set of queries. It protects the developer and the database. You may also notice that the semantics
> of the with statement looks an awful lot like a transaction wrapper. You are correct if you assume so.
>
> As another plus, I also manage the connection. Another thing I noticed that I was doing a lot was ensuring the connection was alive all
> the time and that my code reconnected to the database as soon as it came back alive. So a decision I made was to test the database's connection
> at the start of the transaction. If the database is not connected, the application will attempt to connect to the database every
> `connection_retry_interval` seconds (which is configurable). I also block until the connection comes back alive. The program can still be
> signaled and can exit, but doing this will prevent a ton of unnecessary exceptions from happening.
>
> Now you may ask what happens if the connection is lost during a transaction. This was something I have not found a way to handle, so
> what will happen is an exception will get thrown from the `cursor.execute` which can be caught outside the with statement.

## Code changes

> If there are any requests for changes or additions to be made, submit an issue and/or send a pull request and I will consider the change.
> The functions I have wrapped already are things that I found fairly annoying and not very elegant in psycopg2, but I am sure
> there are many more things that could be done to improve upon the system!

``` Python
from autopg import autopg

db = autopg(dbname='dbname', user='user', password='password')
with db as cursor:
    cursor.execute(
        'UPDATE schema.table SET label = %(value)s WHERE tag = %(tag)s',
        {
            'value': 'myvalue',
            'tag': 'mytag'
        }
    )
    _resultset = cursor.execute('SELECT * FROM schema.table WHERE label = %s', ('my_label',))
    for result in _resultset:
        print(result)
```

> Another simple example is using autopg among multiple processes with safe and unsafe executions. Using this example,
> you will see that while `f1` executes its selects almost immediately, `f2` and `f3` will go hand-in-hand waiting for
> each other to finish before executing their respective transaction. You may also note that the transaction in `f1`
> is not committed at any point. You may also notice that the with statement in `f2` has "with db.safe as cursor" while
> the one in `f3` has "with db as cursor". These mean exactly the same thing.

``` Python
from autopg import autopg
from autopg import namedlock
from multiprocessing import Process

def f1():
    db = autopg(dbname='dbname', user='user', password='password', lock_factory=namedlock, lock_args=('mylock',))
    for i in range(3):
        with db.unsafe as cursor:
            print('unsafe cursor!')
            for r in cursor.execute('select * from schema.table'):
                print(r)
        print('exited unsafe cursor!')

def f2():
    db = autopg(dbname='dbname', user='user', password='password', lock_factory=namedlock, lock_args=('mylock',))
    for i in range(3):
        with db.safe as cursor:
            print('started safe transaction f2')
            for r in cursor.execute('select * from schema.table where id=20'):
                print(r)
            print('ending safe transaction f2')

def f3():
    db = autopg(dbname='dbname', user='user', password='password', lock_factory=namedlock, lock_args=('mylock',))
    for i in range(3):
        with db as cursor:
            print('started safe transaction f3')
            cursor.execute('update schema.table set tag=%s where id=20', ('mytag',))
            print('ending safe transaction f3')

p1 = Process(target=f1)
p2 = Process(target=f2)
p3 = Process(target=f3)

p2.start()
p3.start()
p1.start()
```

## Documentation

> Documentation is created with Sphinx. I do not have a website right now because I am a poor college student, so I have
> the documentation source in this tree. If this class ever gets larger and more widely used, I will consider
> making a website. I will also be adding this class to the python package index when its out of its alpha stage.