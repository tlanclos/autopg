from __future__ import print_function

__author__ = 'taylor'

try:
    import os
    import fcntl
    import errno
except Exception as e:
    print(e)
    exit(-errno.EIO)


class namedlock(object):
    """
    Create a named lock that simply uses a file as a lock

    :param filename: filename/filepath to use as a lock
    """

    def __init__(self, filename):
        self._filename = filename
        self._handle = open(self._filename, 'w+')

    def __del__(self):
        """
        On delete, make sure the handle is closed
        """

        if self._handle:
            self._handle.close()

    def __enter__(self):
        """
        acquire the lock on "with"

        :return: None
        """

        self.acquire()
        return None

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Release the lock when finished

        :param exc_type: exception type
        :param exc_val: exception value
        :param exc_tb: exception traceback
        """

        self.release()

    def acquire(self):
        """
        Acquire the lock and block if lock is already taken
        """

        fcntl.flock(self._handle, fcntl.LOCK_EX)

    def release(self):
        """
        Release the lock and let others use the resource
        """

        fcntl.flock(self._handle, fcntl.LOCK_UN)