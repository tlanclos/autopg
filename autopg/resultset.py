from __future__ import print_function
__author__ = 'taylor'

try:
    import errno
except ImportError as e:
    print(e)
    exit(-errno.EIO)


class resultset(object):
    """
    A result set is an iterable set that allows one to, well iterate through

    :param cursor: a cursor object that has the ability to fetch one row of the result set at a time.
        That means the function `fetchone()` must be defined
    """

    def __init__(self, cursor):
        self._cursor = cursor

    def next(self):
        """
        Fetch the next result

        :return: a result from the cursor
        """

        return self._cursor.fetchone()

    @property
    def rows(self):
        """
        Get the number of remaining rows in the cursor's resultset

        :return: the number of rows in the cursor's resultset
        """

        return self._cursor.rowcount

    def __iter__(self):
        """
        Generator to get the next result in the resultset

        :return: the next result in the resultset
        """

        while True:
            n = self.next()
            if not n:
                break
            yield n