from __future__ import print_function

__author__ = 'taylor'

try:
    import psycopg2
    import errno
    from psycopg2.extras import DictCursor
    from resultset import *
except ImportError as e:
    print(e)
    exit(-errno.EIO)


class dictcursor(DictCursor):
    """
    Wrapper around the dictionary cursor extension of psycopg2 making it more explosion proof

    :param args: same arguments as psycopg2 DictCursor
    :param kwargs: same keyword arguments as psycopg2 DictCursor
    """

    def __init__(self, *args, **kwargs):
        super(dictcursor, self).__init__(*args, **kwargs)

    def execute(self, query, vars=None):
        """
        Execute an SQL statement using the standard cursor's execute, but return an iterable result set

        :param query: query to run
        :param vars: variables to format (can be name value pairs or just a plain tuple)
        :return: an iterable resultset
        """

        super(dictcursor, self).execute(query, vars=vars)
        return resultset(self)

    def fetchall(self):
        """
        Redefine fetchall to return None if the result set was empty

        :return: the results if there was not a ProgrammingError, None otherwise
        """

        try:
            _results = super(dictcursor, self).fetchall()
        except psycopg2.ProgrammingError as e:
            _results = None
        return _results

    def fetchone(self):
        """
        Redefine fetchone to return None if the result set was empty

        :return: the results if there was not a ProgrammingError, None otherwise
        """

        try:
            _results = super(dictcursor, self).fetchone()
        except psycopg2.ProgrammingError as e:
            _results = None
        return _results

    def __enter__(self):
        """
        Define the enter statment just so this class can now be used with a python with statment.

        :return: this cursor
        """

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Does nothing at the moment.

        :param exc_type: exception type
        :param exc_val: exception value
        :param exc_tb: exception traceback
        :return: nothing
        """

        pass