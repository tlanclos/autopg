from __future__ import print_function

__author__ = 'taylor'

try:
    import cursors
    import psycopg2
    import errno
    import time
    from copy import deepcopy
    from psycopg2.extras import DictCursor
    from psycopg2.extensions import ISOLATION_LEVEL_READ_COMMITTED
    from multiprocessing.dummy import Lock
except ImportError as e:
    print(e)
    exit(-errno.EIO)


class autopg(object):
    """
    Thread safe wrapper around psycopg2

    **Required arguments**

    :param dbname: the database name to connect to
    :param user: user to log into the database with

    **Database configuration**

    :param host: host name/ip to use to connect to the database

        :default: psycopg2 default

    :param port: port used to connect to the database

        :default: psycopg2 default

    :param password: password used to log into the database

        :default: psycopg2 default

    :param application_name: application name that will be set in postgresql's database (i.e. pg_stat_activity)

        :default: no name

    :param connection_retry_interval: if attempting to connect in block mode (default), a connection attempt will occur
        every specified seconds

        :default: 10

    **Logger options**

    :param logger_function: gets called when a generic message needs to be logged

        :default: print

    :param info_logger_function: gets called when information needs to get logged and logs a corresponding message

        :default: logger_function

    :param error_logger_function: gets called when an error occurs and logs a corresponding message

        :default: logger_function

    :param warning_logger_function: gets called when a warning occurs and logs a corresponding message

        :default: logger_function

    :param critical_logger_function: gets called when a critical error occurs and logs a corresponding message

        :default: logger_function

    :param debug: whether or not to display/log messages

        :default: False

    **Class configuration**

    :param lock_factory: lock class used to lock the class and keep it thread safe. If specified, the class must
        support python's "with" keyword to lock the class and must contain the functions "acquire()"
        and "release()". A good use for specifying this class is if a multiprocessing IPC Lock is
        specified. The parameters would need to point to lock file descriptor somehow

        :default: multiprocessing.dummy.Lock

    :param lock_args: args to pass into the custom lock function

        :default: tuple()

    :param lock_kwargs: keyword args to pass into the custom lock function

        :default: dict()

    :param block_default: this essentially will convert the class from fully managed to partially managed. When
        set to True, the class will always test the database's connection and block until a
        connection is made. If set to False, it will be possible to receive a None cursor when
        querying with python's "with" statement. This comes from the fact that if one does not
        block a connection attempt, it will be unsafe to attempt to access psycopg2's connection

        :default: True

    """

    def __init__(self, dbname, user, **config):
        self._database = dbname
        self._user = user
        self._host = config.get('host')
        self._port = config.get('port')
        self._password = config.get('password')
        self._application_name = config.get('application_name', self.__class__.__name__)

        self._connection_retry_interval = config.get('connection_retry_interval', 10)

        self._logger = config.get('logger_function', print)
        self._infologger = config.get('info_logger_function', self._logger)
        self._errorlogger = config.get('error_logger_function', self._logger)
        self._warnlogger = config.get('warning_logger_function', self._logger)
        self._critlogger = config.get('critical_logger_function', self._logger)
        self._debug = config.get('debug', False)

        self._log('info', 'Initializing')

        _lockargs = config.get('lock_args', ())
        _lockkwargs = config.get('lock_kwargs', {})
        self._access = config.get('lock_factory', Lock)(*_lockargs, **_lockkwargs)
        self._connection = None
        self._blockdefault = config.get('block_default', True)

        self._safe_execution = True

        self._log('info', 'Initialized')

    def _log(self, type, message):
        """
        Logger function utilized throught the class to log info, errors, warnings, and generic logs.

        :param type: message type (case insensitive) {info, error, warn, crit, ...} anything else will
                    use default self._logger
        :param message: message to display
        :return:
        """

        if self._debug:
            _type = type.lower()
            _msg = '{name}: {message}'.format(name=self._application_name, message=message)
            if _type == 'info':
                self._infologger(_msg)
            elif _type == 'error':
                self._errorlogger(_msg)
            elif _type == 'warn':
                self._warnlogger(_msg)
            elif _type == 'crit':
                self._critlogger(_msg)
            else:
                self._logger(_msg)

    @property
    def connected(self):
        """
        Property that checks if the current connection to the database is valid

        :return: True or False regarding the current connection state to the database
        """

        self._log('info', 'Testing connection')

        if self._connection is None:
            return False

        try:
            _cursor = self._connection.cursor(cursor_factory=DictCursor)
            _cursor.execute('SELECT 1 AS result;')
            _result = _cursor.fetchone()
            if _result['result'] == 1:
                self._log('info', 'Database connected')
                return True
            return False
        except Exception as e:
            self._log('warn', 'Database was disconnected')
            return False

    @property
    def safe(self):
        """
        Use this when you want to explicity lock the connection. Note: this is the default if not specifed.

        :return: This autopg object locked
        """

        return self

    @property
    def unsafe(self):
        """
        Use this when you want to explicity unlock the connection. Note: this should only be used if required.
        A new cursor will be returned from this property. Because this type is considered unsafe and should only be
        used for SQL SELECTs (that do not need to be committed), the commit will not happen automatically and must be
        done manually after the "with" statement.

        :return: A new cursor.
        """

        self.unsafe_connect()

        if self._connection is not None:
            self._connection.set_isolation_level(ISOLATION_LEVEL_READ_COMMITTED)
            return self._connection.cursor(cursor_factory=cursors.dictcursor)
        return cursors.dictcursor(conn=self._connection)

    def _connect(self):
        """
        A single attempt to connect to the database, this function should only be used internal to this class
        and the class should preferably block (by default) until the connection has been created

        :return: whether or not the connection was successful
        """

        if not self.connected:
            self._connection = psycopg2.connect(
                database=self._database,
                user=self._user,
                port=self._port,
                host=self._host,
                password=self._password,
                application_name=self._application_name
            )
        return self.connected

    def unsafe_connect(self, block=None):
        """
        Run an explicit connect to the database. This module attempts to connect to the database when needed, so
        it is recommended to let the module handle the connection for you. This function, unlike the safe connect
        function :py:func:`connect` does not grab the lock before testing the connection.

        :param block: whether or not to block until connected. If block is None, then self._blockdefault is used
        :return: whether or not the connection attempt is successful. If blocking, should always be successful
        """

        self._log('info', 'Attempting to connect')

        if block is None:
            block = self._blockdefault

        if block:
            _connected = self._connect()
            while not _connected:
                self._log('warn', 'Connection failed')
                time.sleep(self._connection_retry_interval)
                _connected = self._connect()
            return _connected
        else:
            return self._connect()

    def connect(self, block=None):
        """
        Run an explicit connect to the database. This module attempts to connect to the database when needed, so
        it is recommended to let the module handle the connection for you.

        :param block: whether or not to block until connected. If block is None, then self._blockdefault is used
        :return: whether or not the connection attempt is successful. If blocking, should always be successful
        """

        with self._access:
            return self.unsafe_connect(block=block)

    def __enter__(self):
        """
        Performs the enter operation for python's "with" statement. This will attempt to connect to the database
        using the default block operation, acquire a lock on this connection, ensure that we are in
        ISOLATION_LEVEL_READ_COMMITTED mode, and create a cursor off the connection

        :return: a new cursor
        """

        self.connect()
        self._log('info', 'Entering isolated safe execution mode')
        self._access.acquire()

        if self._connection is not None:
            self._connection.set_isolation_level(ISOLATION_LEVEL_READ_COMMITTED)
            return self._connection.cursor(cursor_factory=cursors.dictcursor)
        return cursors.dictcursor(conn=self._connection)

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Performs an exit operation for python's "with" statement. If an exception occurs and its a psycopg2 error,
        the connection rolls back any changes made during the "with" and logs the traceback. Otherwise, the connection
        commits any changes made during the "with".

        :param exc_type: Exception type that occurred, if any
        :param exc_val: Exception value, if an exception occurred
        :param exc_tb: Traceback provided by the exception
        :return: nothing
        """

        self._log('info', 'Exiting isolated safe execution mode')

        if exc_type:
            if issubclass(exc_type, psycopg2.Error):
                self._log('error', 'Psycopg2 Exception: {err}'.format(err=exc_val))
                self._log('crit', '{tb}'.format(tb=exc_tb))

            if self.connected:
                self._log('info', 'Rolling back changes')
                self._connection.rollback()
            else:
                self._log('warn', 'Database disconnected during transaction')
        else:
            if self.connected:
                if self._safe_execution:
                    self._log('info', 'Committing changes')
                    self._connection.commit()
            else:
                self._log('warn', 'Database disconnected during transaction')

        self._access.release()