from __future__ import print_function

__author__ = 'taylor'

try:
    import errno
except Exception as e:
    print(e)
    exit(-errno.EIO)

if __name__ == '__main__':
    from autopg import autopg
    from autopg import namedlock
    from multiprocessing import Process

    def f1():
        db = autopg(dbname='dbname', user='user', password='password', lock_factory=namedlock, lock_args=('mylock',))
        for i in range(3):
            with db.unsafe as cursor:
                print('unsafe cursor!')
                for r in cursor.execute('select * from schema.table'):
                    print(r)
            print('exited unsafe cursor!')

    def f2():
        db = autopg(dbname='dbname', user='user', password='password', lock_factory=namedlock, lock_args=('mylock',))
        for i in range(3):
            with db.safe as cursor:
                print('started safe transaction f2')
                for r in cursor.execute('select * from schema.table where id=20'):
                    print(r)
                print('ending safe transaction f2')

    def f3():
        db = autopg(dbname='dbname', user='user', password='password', lock_factory=namedlock, lock_args=('mylock',))
        for i in range(3):
            with db as cursor:
                print('started safe transaction f3')
                cursor.execute('update schema.table set tag=%s where id=20', ('mytag',))
                print('ending safe transaction f3')

    p1 = Process(target=f1)
    p2 = Process(target=f2)
    p3 = Process(target=f3)

    p2.start()
    p3.start()
    p1.start()